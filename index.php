<?php error_reporting(-1); ini_set('display_errors', 'On'); ?>
<?php require('db.php'); ?>

<html>

    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>        
    </head>

<body>    

<!-- Regiones -->
<label for="selectRegion">Región</label>
<select id="selectRegion">
    <option>Seleccione Región</option>    
<?php
if ($regiones = $mysqli->query("SELECT * FROM region;")) {
    while ($r = $regiones->fetch_array()) {
        $nombreRegion = htmlentities($r[1], ENT_QUOTES, 'iso-8859-1');
        echo "<option value=\"{$r[0]}\">{$nombreRegion}</option>";
    }
}
?>
</select>

<form action="index.php" method="GET" id="formRegion">
    <input type="hidden" name="regionId" />
</form> 

<!-- Provincias -->
<label for="selectProvincia">Provincia</label>
<select id="selectProvincia">
    <option>Seleccione Provincia</option>    
<?php
if ($provincias = $mysqli->query("SELECT * FROM provincia;")) { // WHERE PROVINCIA_REGION_ID = REGION_ID seleccionada ...
    while ($p = $provincias->fetch_array()) {
        $nombreProvincia = htmlentities($p[1], ENT_QUOTES, 'iso-8859-1');
        echo "<option value=\"{$p[0]}\">{$nombreProvincia}</option>";    
    }
}
?>
</select>

<!-- Comunas -->
<label for="selectComuna">Comuna</label>
<select id="selectComuna">
    <option>Seleccione Comuna</option>    
<?php
if ($comunas = $mysqli->query("SELECT * FROM comuna")) { // WHERE COMUNA_PROVINCIA_ID = PROVINCIA_ID seleccionada ...
    while ($c = $comunas->fetch_array()) {
        $nombreComuna = htmlentities($c[1], ENT_QUOTES, 'iso-8859-1');
        echo "<option value=\"{$c[0]}\">{$nombreComuna}</option>";    
    }
}
?>
</select>
</body>

</html>